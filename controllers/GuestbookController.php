<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 27.07.2017
 * Time: 20:46
 */

namespace app\controllers;

use Yii;
use app\models\GuestbookSearch;
use yii\web\Controller;
use app\models\CreateReview;
use yii\web\UploadedFile;

class GuestbookController extends Controller
{


    /**
     * Lists all Country models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GuestbookSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,

        ]);
    }

    public function actionUpload()
    {
        $model = new CreateReview();
        if (Yii::$app->request->isPost) {
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');

            if ($model->upload()) {

                return true;
            }
        }

      //  return $this->render('upload', ['model' => $model]);
    }

    public function actionCreateReview()
    {
        $model = new CreateReview();
        if ($model->load(Yii::$app->request->post()) ) {
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');

            if ($model->validate() && $model->upload() && $model->create()) {

                $searchModel = new GuestbookSearch();
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

                return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,

                ]);

            }
        }

        $this->actionIndex();
    }



}
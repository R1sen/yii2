<?php

namespace app\components;

use Yii;
use yii\base\Widget;
use app\models\LoginForm;
use yii\debug\models\search\Log;

/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 02.08.2017
 * Time: 18:40
 */
class LoginFormWidget extends Widget
{

    public function init()
    {
        Yii::info('1');
        Yii::info($this->getViewPath());
    }

    public function run() {
        Yii::info('1');
        if (Yii::$app->user->isGuest) {
            $model = new LoginForm();
            return $this->render('loginFormWidget', [
                'model' => $model,
            ]);
        } else {
            $model = new LoginForm();
            return $this->render('loginFormWidget', [
                'model' => $model,
            ]);
        }
    }
}
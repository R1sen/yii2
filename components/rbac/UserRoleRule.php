<?php

namespace app\components\rbac;

use app\models\User;
use yii\helpers\ArrayHelper;
use yii\rbac\Rule;

class UserRoleRule extends Rule
{
    public $name = 'userRole';
    public function execute($user, $item, $params)
    {

        $user = ArrayHelper::getValue($params, 'user', User::findOne($user));

        if ($user) {
            $access = $user->access;
            if ($item->name === 'admin') {
                return $access == User::ROLE_ADMIN;
            }
            elseif ($item->name === 'user') {
                return $access == User::ROLE_ADMIN || $access == User::ROLE_USER;
            }
        }
        return false;

    }
}
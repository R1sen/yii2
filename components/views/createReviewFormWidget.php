<?php

/* @var $model app\models\CreateReview */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;

    Modal::begin([
        'header'=>'<h4>Создание отзыва</h4>',
        'id'=>'create-modal',
    ]);
    ?>

    <p>Пожалуйста, заполните следующие поля для создания отзыва:</p>

    <?php $form = ActiveForm::begin([
        'id' => 'create_form',
        'action' => ['guestbook/create-review'],
        'options' => ['enctype' => 'multipart/form-data']
    ]);

    echo $form->field($model, 'name')->textInput()->label('Имя');
    echo $form->field($model, 'imageFile')->fileInput()->label('Изображение');
    echo $form->field($model, 'body')->textInput()->label('Текст');

    ?>

    <div class="form-group">
        <div class="text-right">

            <?php
            echo Html::button('Отмена', ['class' => 'btn btn-default', 'data-dismiss' => 'modal']);
            echo Html::submitButton('Создать', ['class' => 'btn btn-primary', 'name' => 'create-review-button']);
            ?>

        </div>
    </div>

<?php

ActiveForm::end();
Modal::end();
?>
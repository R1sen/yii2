<?php

namespace app\components;

use Yii;
use yii\base\Widget;
use app\models\CreateReview;

/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 02.08.2017
 * Time: 18:40
 */
class СreateReviewFormWidget extends Widget
{

    public function run()
    {
        $model = new CreateReview();
        return $this->render('createReviewFormWidget', [
            'model' => $model,
            ]);
    }
}
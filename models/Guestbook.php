<?php

namespace app\models;


use yii\db\ActiveRecord;

class Guestbook extends ActiveRecord
{
    public function attributeLabels()
    {
        return [
            'name' => 'Name',
            'date' => 'Date',
            'image' => 'Image',
            'body' => 'Text',

        ];
    }

  /*  public function getImageurl()
    {
        return \Yii::$app->urlManager->createUrl('@web/path/to/logo/'.$this->image);
    }*/


}
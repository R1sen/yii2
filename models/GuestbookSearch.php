<?php

namespace app\models;


use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;

class GuestbookSearch extends Guestbook
{

    public $demo;

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }


    public function search($params)
    {
        $query = Guestbook::find();

        // add conditions that should always apply here

        $pages = new Pagination(['defaultPageSize' => 5, 'totalCount' => $query->count()]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => $pages,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

/*        // grid filtering conditions
        $query->andFilterWhere([
            'population' => $this->population,
        ]);

        $query->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'name', $this->name]);*/

        return $dataProvider;
    }


}
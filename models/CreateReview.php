<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 03.08.2017
 * Time: 16:06
 */

namespace app\models;


use Faker\Provider\DateTime;
use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class CreateReview extends Model
{
    public $name;
    public $date;
    public $body;

    /**
     * @var UploadedFile
     */
    public $imageFile;

    public function rules()
    {
        return [
            [['name', 'body'], 'required'],
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],
        ];
    }

    public function upload()
    {
            $this->imageFile->saveAs('images/guestbook/' . $this->imageFile->baseName . '.' . $this->imageFile->extension);
            return true;
    }

    public function create()
    {
            Yii::$app->db->createCommand()->insert('guestbook', [
                'name' => $this->name,
                'date' => time(),
                'image' => $this->imageFile->name,
                'body' => $this->body
            ])->execute();

            return true;
    }


}
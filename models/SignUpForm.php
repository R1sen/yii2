<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class SignUpForm extends Model
{
    public $username;
    public $password;
    public $password_repeat;
    public $email;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            //Asynchronous check username
            ['username', 'in', 'range' => User::find()->select('username')->asArray()->column(), 'strict' => true,
                'not' => true, 'message' => 'Please choose another username.'],
            [['username', 'password', 'email'], 'required'],
            ['password_repeat', 'compare', 'compareAttribute' => 'password'],
            ['email', 'email'],

        ];
    }

    public function register()
    {

        if($this->validate()) {
            Yii::$app->db->createCommand()->insert('users', [
                'username' => $this->username,
                'password' => Yii::$app->getSecurity()->generatePasswordHash($this->password),
                'email' => $this->email
            ])->execute();

            return true;
        } else
            return false;

    }
}

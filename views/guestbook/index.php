<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;
use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;
use app\components\СreateReviewFormWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\GuestbookSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $image app\models\Guestbook */
/* @var $model app\models\GuestbookSearch */

$this->title = 'Guestbook';
$this->params['breadcrumbs'][] = $this->title;
echo СreateReviewFormWidget::widget();
?>
<div class="country-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>



    <?php $form = ActiveForm::begin();

    echo $form->field($searchModel, 'demo', [
        'inputTemplate' =>
            '<div class="input-group">{input}
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button">Go!</button>
                </span>
            </div>',
    ])->label('Поиск')->textInput(['placeholder' => 'Введите текст для поиска...',]);

    ActiveForm::end();
    ?>


    <?= Html::submitButton('Создать отзыв', ['class' => 'btn btn-primary', 'name' => 'create-review-button', 'url' => '#', 'data-toggle' => 'modal', 'data-target' => '#create-modal']) ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            'date' => [
                'attribute' => 'date',
                'value' => function ($data) {
                    return Yii::$app->formatter->asDate($data['date']);
                },
            ],
            'image' => [
                'attribute' => 'image',
                'format' => 'html',
                'value' => function ($data) {
                    return Html::img(Yii::getAlias('@web') . '/images/guestbook/' . $data['image'], ['width' => '70px']);
                },

            ],
            'body',

            ['class' => 'yii\grid\ActionColumn', 'visible' => Yii::$app->user->can('admin')]

        ],
    ]);

    ?>

    <?= Html::submitButton('Создать отзыв', ['class' => 'btn btn-primary', 'name' => 'create-review-button', 'url' => '#', 'data-toggle' => 'modal', 'data-target' => '#create-modal']) ?>

</div>
